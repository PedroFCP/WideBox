package functions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import baseClasses.Theaters;

public class Functions {
	
	// file format will be Theater, Seat, A/O/R(Available, Occupied, Reserved), Owner)

	String file = "Theaters/Theater";
	
	BufferedReader br = null;
	
	public Functions(int machineID) {
		file = file + machineID + ".csv";
	}
	
	public ArrayList<ArrayList<String>> getTheater(int theaterID) {
		String line;
		ArrayList<ArrayList<String>> theater = new ArrayList<ArrayList<String>>();
		boolean done = false;
		try {
			br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null && !done) {
				String[] entry = line.split(",");
				ArrayList<String> singleList = new ArrayList<String>();
				if (Integer.parseInt(entry[0]) == theaterID) {
					singleList.add(entry[1]);
					singleList.add(entry[2]);
					theater.add(singleList);
				}
				else if (Integer.parseInt(entry[0]) > theaterID) {
					done = true;
				}
				
			}
		}
		catch (Exception E) {
			System.out.println("Could not get the Theater.");
		}
		return theater;
	}
	
	
}
