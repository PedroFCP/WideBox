package remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import server.Seat;
import server.Theaters;

public interface RemoteFunction extends Remote {
	
	public List<Theaters> search() throws RemoteException;
	public List<Seat> query(int theaterID) throws RemoteException;
	public boolean accept() throws RemoteException;
	public boolean reserve(int seatID) throws RemoteException;
	public boolean cancel() throws RemoteException;
	
}
