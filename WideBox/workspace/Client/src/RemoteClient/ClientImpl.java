package RemoteClient;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ClientImpl extends UnicastRemoteObject implements ClientFunctions{

	public ClientImpl() throws RemoteException{
		
	}
	
	@Override
	public Registry Connect() throws RemoteException {
		// TODO Auto-generated method stub
		Registry registry = null;
		try {
			registry = LocateRegistry.getRegistry("127.0.0.1", 5000);
			
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
		return registry;
	}

}
