package RemoteClient;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;

public interface ClientFunctions{

	public Registry Connect() throws RemoteException;
}
